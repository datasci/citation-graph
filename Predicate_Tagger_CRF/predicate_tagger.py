# -*- coding: utf-8 -*-
import sklearn
import pandas as pd
import numpy as np
import os
from collections import Counter
from functions_predicate_tagger import data_load, train_tag, train_tag_reportf1, ShortenContext

#%%

input_file = './m_predicate_data.csv'

# The 'data' format is list [ [row1],... ]
# in rowi each citation is replaced with C1CITE and rowi = [ ('word','POS', 'label'), ...]
# predicate labele is one of: {'B_PRD','I_PRD','O'}

data=data_load(input_file)

#%%

data=data[1:1000]

#%%

print('final number of rows good for proceeding',len(data))

#%%
from sklearn.cross_validation import KFold
kf=KFold(len(data), n_folds=10, shuffle=True, random_state=110)#set random state to None 

data=np.array(data)

ff1score=[]

#print(data[0])
#exit()

for train_index, test_index in kf:
    train_data, test_data = data[train_index], data[test_index]
    ff1score.append(list(train_tag_reportf1(train_data,test_data)))
    #break
    
    
ff1score=np.array(ff1score)
ff1score_avr=ff1score.mean(0)
print('ff1score_avr=',ff1score_avr)


#print(ff1score) #all rounds in details	
