# -*- coding: utf-8 -*-
"""
Created on Mon Mar 28 14:16:48 2016

@author: meubuntu
"""

from nltk import tokenize

def ShortenContext(context,citation_text):
    context=context.replace(citation_text, 'C1CITE', 1)
    
    context=context.replace("\\\\n","\\n")
    context=context.replace("\\n\\n","\\n")
    context=context.replace("\\n\\n","\\n") #to remove 3 //ns
    context=context.replace("\\n\\n","\\n") #to remove 4 //ns
    
    split_context=context.split("\\n")
    split_context=[s for s in split_context if s!='']
    for i,s in enumerate(split_context):
        if 'C1CITE' in s:
            break
    context=split_context[(i-1,i)[i==0]:(i+1,i)[i==len(split_context)]]   
        
    tmp=''
    for s in context:
        tmp=tmp+s
    context=tmp    
    
    sent_context=tokenize.sent_tokenize(context)
    for i,s in enumerate(sent_context):
        if 'C1CITE' in s:
            break
        
    context = sent_context[(i-1,i)[i==0]:(i+1,i)[i==len(sent_context)]]
    
    tmp=''
    for s in context:
        tmp=tmp+s
    context=tmp
    
    if len(context)>3000:
        place_idx=context.find(citation_text)
        if (place_idx>2000 & place_idx<len(context)-500):
            context=context[place_idx-2000:place_idx+500]
        elif(place_idx<2000):
            context=context[:place_idx+500]
        elif(place_idx>len(context)-500):
            context=context[place_idx-2000:]
            

    context=context.replace('C1CITE',citation_text, 1)
    
    return [context,citation_text]
    
#%%
import numpy as np

def data_load(input_file):
    load_saved=input('for loading pre-tagged data enter "1", for tagging again enter "0": ')
    if load_saved=='0':        
        loaded_data=load_tagged_data(input_file) #load the expert tagged data
        #print('len of data:',len(loaded_data))
        data=convert_format_data(loaded_data)
        
        np.save('tagged'+input_file.split('/')[-1][:-4]+'_genby_convert_format_data',data)
        
    elif load_saved=='1':
        print('loading tagged data...')
        data=np.load('tagged'+input_file.split('/')[-1][:-4]+'_genby_convert_format_data.npy')
    
    else:
        print('wrong input')
        data=data_load(input_file)
        
    return data


#%%
import pandas as pd
def load_tagged_data(file_name):
    # ' ','Filename','Context','Original_Citation_Text','Predicate','Label','Confidence'
    data=pd.read_csv(file_name)
    context=data['Context'].tolist()
    original_Citation_Text=data['Original_Citation_Text'].tolist()
    predicate=data['Predicate'].tolist()

    #TODO check if each line has a predicate
    PRED=[i for i in predicate if True] 
    CONTEXT=[i for i in context if True]
    ORIGCITE=[i for i in original_Citation_Text if True]

    data=zip(CONTEXT,ORIGCITE,PRED)
    data=list(data)
    return data


#%%
import nltk
import re
from nltk.tokenize import TweetTokenizer
tknzr = TweetTokenizer(reduce_len=True) #may want to turn reduce_len off

def convert_format_data(data):
    tagged_data=[]
    for cont, orig_cite, pred in data:
        cont=' '+cont
        cont_full=cont
        
        cont=cont.replace('(',' ',500)
        cont=cont.replace(')',' ',500)
        cont=cont.replace('\\',' ',500)
        cont=cont.replace('[',' ',500)
        cont=cont.replace(']',' ',500)
        #cont=re.sub('\d', '', cont)
        
        orig_cite=orig_cite.replace('(',' ',500)
        orig_cite=orig_cite.replace(')',' ',500)
        orig_cite=orig_cite.replace('\\',' ',500)
        orig_cite=orig_cite.replace('[',' ',500)
        orig_cite=orig_cite.replace(']',' ',500)
        #orig_cite=re.sub('\d', '', orig_cite) 
        
        pred=pred.replace('(',' ',500)
        pred=pred.replace(')',' ',500)
        pred=pred.replace('\\',' ',500)
        pred=pred.replace('[',' ',500)
        pred=pred.replace(']',' ',500)
        #pred=re.sub('\d', '', pred)
        
        cont=cont.replace(' '+orig_cite,' cC1CITE ',100)#when working with law-student's data
        #we should only mark the first one and withought ' '.

        cont=re.sub('\d', '', cont)
        cont=cont.replace(' cCCITE ',' C1CITE ', 1)
        pred=re.sub('\d', '', pred)

        pred=tknzr.tokenize(pred)
        cont=tknzr.tokenize(cont)

        if 'C1CITE' in cont:
            cite_position=cont.index('C1CITE')
        else:
            #print('\n-------ERROR: no CITATION (will not process this row)--------')
            #print(cont_full,'\n')
            #print(cont)
            #print(orig_cite,'\n')
            continue

        cont=nltk.pos_tag(cont)

        pred_post=-1
        flag=0
        i=cite_position
        while i>=len(pred)-1:
            if cont[i][0]==pred[-1]:
                if flag==1:
                    cont[i]=cont[i]+('O',)
                    i=i-1
                    continue
                flag=1
                for j in range(1,len(pred)):
                    if cont[i-j][0]!=pred[-j-1]:
                        flag=flag*0
                if flag==1:
                    pred_post=i-len(pred)+1
                    cont[i-len(pred)+1]=cont[i-len(pred)+1]+('B_PRD',)
                    for j in range(1,len(pred)):
                        cont[i-len(pred)+1+j]=cont[i-len(pred)+1+j]+('I_PRD',)
                    i=i-len(pred)+1
                else:
                    cont[i]=cont[i]+('O',)
            elif cont[i][0]=='C1CITE':
                #cont[i]=cont[i]+('O-CITE',)
                cont[i]=cont[i]+('O',)
            else:
                cont[i]=cont[i]+('O',)
            i=i-1
        for i in range(len(pred)-1):
            if len(cont[i])==2:
                cont[i]=cont[i]+('O',)
        for i in range(cite_position+1,len(cont)):
            cont[i]=cont[i]+('O',)

        if pred_post<0:
            #print('\n-------ERROR: no PREDICATE (will not process this row)--------')
            #print(cont_full,'\n')
            #print('citation=',cite_position,'*'+orig_cite,'\n')
            #print('predicate=',' '.join(pred),'\n')
            #print(cont)
            #input('press enter')
            continue

        cont.append(('PRED_POST',pred_post))
        #append the index of the citation, used to pick the correct predicate
        cont.append(('CITE_POST',cite_position))
        
        tagged_data.append(cont)
    return tagged_data

#%%

def word2features(sent, i):
    word = sent[i][0]
    postag = sent[i][1]
    features = [
        'bias',
        'word.lower=' + word.lower(),
        'word.isdigit=%s' % word.isdigit(),
        'postag=' + postag,
        'postag[:1]=' + postag[:1],
        'postag[-2:]=' + postag[-2:],
        'Dc-w<=0 =%s' % ( (sent[-1][1]-i)<=0 ),
        'Dc-w=1 =%s' % ((sent[-1][1]-i)==1),
        'Dc-w>4=%s' % ((sent[-1][1]-i)>3),
        'Dc-w>10=%s' % ((sent[-1][1]-i)>10),
        'Dc-w>18=%s' % ((sent[-1][1]-i)>18),
    ]
    if i > 0:
        word1 = sent[i-1][0]
        postag1 = sent[i-1][1]
        features.extend([
            '-1:word.lower=' + word1.lower(),
            '-1:postag=' + postag1,
            '-1:postag[:1]=' + postag1[:1],
            '-1:postag[-2:]' + postag1[-2:],
            '-1:word.isdigit=%s' % word1.isdigit()
        ])
    #else:
        #features.append('BOS')

    if i < len(sent)-3:
        word1 = sent[i+1][0]
        postag1 = sent[i+1][1]
        features.extend([	
            '+1:word.lower=' + word1.lower(),
            '+1:postag=' + postag1,
            '+1:postag[:1]=' + postag1[:1],
            '+1:postag[-2:]' + postag1[-2:],
            '+1:word.isdigit=%s' % word1.isdigit()
        ])
    #else:
        #features.append('EOS')

    return features


def sent2features(sent):
    return [word2features(sent, i) for i in range(len(sent)-2)]

def sent2labels(sent):
    sent=sent[:-2]
    return [label for token, postag, label in sent]

def sent2tokens(sent):
    sent=sent[:-2]
    return [token for token, postag, label in sent]
    
#%%
import pycrfsuite
from sklearn.preprocessing import LabelBinarizer
from itertools import chain
from sklearn.metrics import classification_report, confusion_matrix, f1_score

def create_classification_report(y_true, y_pred):
    
    lb = LabelBinarizer()
    y_true_combined = lb.fit_transform(list(chain.from_iterable(y_true)))
    y_pred_combined = lb.transform(list(chain.from_iterable(y_pred)))

    #tagset = set(lb.classes_) - {'O'}
    tagset = set(lb.classes_)
    tagset = sorted(tagset, key=lambda tag: tag.split('-', 1)[::-1])
    class_indices = {cls: idx for idx, cls in enumerate(lb.classes_)}
    
    ffscore=f1_score(y_true_combined, y_pred_combined, labels=[class_indices[cls] for cls in tagset], average=None)
    lbls=[class_indices[cls] for cls in tagset]
    
    print(classification_report(y_true_combined, y_pred_combined, labels = lbls, target_names = tagset) )
    print(tagset)
    
    return ffscore

def print_transitions(trans_features):
    for (label_from, label_to), weight in trans_features:
        print("%-6s -> %-7s %0.6f" % (label_from, label_to, weight))
        
def print_state_features(state_features):
    for (attr, label), weight in state_features:
        print("%0.6f %-6s %s" % (weight, label, attr)) 

def train_tag_reportf1(train_data, test_data):

    fTrain_crf(train_data)

    y_pred, y_test = fTag_crf(test_data)

    fPrintErr(y_test, y_pred, test_data)

    ff1score = fGen_report(y_test, y_pred)

    return ff1score
    
def fTrain_crf(train_data):
    print('starting training')
    X_train= [sent2features(s) for s in train_data]
    y_train= [sent2labels(s) for s in train_data]
    
    trainer = pycrfsuite.Trainer(verbose=False) #verbose: print debug errors
    
    for xseq, yseq in zip(X_train, y_train):
        trainer.append(xseq, yseq)
    #Note to self: this params were selected with 3-fold-CV on 100 test datas (not in the data set now).
    trainer.set_params({
        'c1': 0.693,   # coefficient for L1 penalty
        'c2': 1.5e-3,  # coefficient for L2 penalty
        'max_iterations': 150,  # stop earlier
    
        # include transitions that are possible, but not observed
        'feature.possible_transitions': True
    })
    trainer.train('usc_edgelist_crfsuite_trainedmodel')
    print('Done training')
    
def fTag_crf(test_data):
    
    X_test= [sent2features(s) for s in test_data]

    y_test= [sent2labels(s) for s in test_data]
    
    tagger = pycrfsuite.Tagger()
    tagger.open('usc_edgelist_crfsuite_trainedmodel')

    y_pred = [tagger.tag(xseq) for xseq in X_test]
    #check if there are more than one 'B_PRD', select the latest
    
    for i in range(len(y_pred)):
        indLastB_PRD = 0
        tmp_y = y_pred[i]
        for j in range(len(tmp_y)):
            if tmp_y[j] == 'B_PRD':
                indLastB_PRD = j

        for j in range(indLastB_PRD):
            tmp_y[j] = 'O'

        y_pred[i] = tmp_y

    return y_pred, y_test
    
def fGen_report(y_test, y_pred):
    
    ff1score=create_classification_report(y_test, y_pred)
    show_common_features=0
    if show_common_features==1:
        info = tagger.info()
        print("Top likely transitions:")
        print_transitions(Counter(info.transitions).most_common(15))
        
        print("\nTop unlikely transitions:")
        print_transitions(Counter(info.transitions).most_common()[-15:])
        
        print("Top positive:")
        print_state_features(Counter(info.state_features).most_common(20))
        
        print("\nTop negative:")
        print_state_features(Counter(info.state_features).most_common()[-20:])
    
    return ff1score
    
def fPrintErr(y_test, y_pred, test_data):

    print('len(test_data)=', len(test_data))
    print('len(y_test)=', len(y_test))

    for i in range(len(y_test)):
	
        if y_test[i]==y_pred[i]:
            continue
	
        print('len(y_test[i])=', len(y_test[i]) )

        tmp_er_sent=[w[0] for w in test_data[i]][:-2]

        er_sent=''

        for j in range(len(tmp_er_sent)-1):

            if y_test[i][j]=='B_PRD':
                er_sent=er_sent+'<PREDICATE>'

            er_sent=er_sent+' '+tmp_er_sent[j]

            if (y_test[i][j]=='B_PRD' or y_test[i][j]=='I_PRD') and y_test[i][j+1]=='O':
                er_sent=er_sent+'<\PREDICATE>'
	
        print('-->',er_sent)

        er_sent=''
        for j in range(len(tmp_er_sent)-1):

            if y_pred[i][j]=='B_PRD':
                er_sent=er_sent+'<PREDICATE>'

            er_sent=er_sent+' '+tmp_er_sent[j]

            if (y_pred[i][j]=='B_PRD' or y_pred[i][j]=='I_PRD') and y_pred[i][j+1]=='O':
                er_sent=er_sent+'<\PREDICATE>'

        print('-->',er_sent)
	
        print(y_test[i])
        print(y_pred[i])
    
    
    
#%%

def train_tag(train_data,test_data):
    X_train= [sent2features(s) for s in train_data]
    y_train= [sent2labels(s) for s in train_data]
    
    X_test= [sent2features(s) for s in test_data]
    
    trainer = pycrfsuite.Trainer(verbose=False) #verbose: print debug errors
    
    for xseq, yseq in zip(X_train, y_train):
        trainer.append(xseq, yseq)
    
        trainer.set_params({
            'c1': 0.1,   # coefficient for L1 penalty
            'c2': 1e-3,  # coefficient for L2 penalty
            'max_iterations': 200,  # stop earlier
        
            # include transitions that are possible, but not observed
            'feature.possible_transitions': True
        })
        
    trainer.train('usc_edgelist_crfsuite_trainermodel')
    tagger = pycrfsuite.Tagger()
    tagger.open('usc_edgelist_crfsuite_trainermodel')

    y_pred = [tagger.tag(xseq) for xseq in X_test]
    return y_pred
