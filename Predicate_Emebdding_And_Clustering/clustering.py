import gensim, numpy, collections
import re, csv, sys
import sklearn.cluster as cluster
from sklearn.metrics.pairwise import cosine_similarity
import logging
import pandas as pd

logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

import warnings
warnings.filterwarnings("ignore", category=DeprecationWarning)
from spacy.en import English
print("Loading spacy tokenizer model")
spacy_nlp = English(parser=False, tagger=False, entity=False)
print ("Done loading spacy model")

#%%

ann_data_file='./397_labeled_samples.csv' #the annotated data

usc_data_file='../usc-edgelist_cleaned.csv' #used to train word2vec

anndf=pd.read_csv(ann_data_file)

#%%

def load_sents(df):#df has the sentences in the context col
    print('building sentence list')
    sent_list=[]
    sents=list(df.Context)
    ctr=0
    for sent in sents:
        sent=clean_sent(sent)
        sent_list.append(sent)
        if ctr%1000==0:
            print(ctr/131000,'complete...')
        ctr+=1
    f=open('cleaned-sents.txt','w')
    for sent in sent_list:
        f.write(str(sent))
        f.write('\n')
    f.close()
    print('Done building list')
    return sent_list
    
def clean_sent(sent):
    sent.lower()
    sent=re.sub(r'[,.\-)(!\d\?;]', r' ', sent)
    sent=' '.join(word for word in sent.split() if len(word)>1)#remove all words of len 1
    spacy_toks = spacy_nlp(sent,tag=False, parse=False, entity=False)
    sent = list()
    for a_tok in spacy_toks:
        sent.append(a_tok.string.strip())
    return sent
        
#%%

stoplist= set('the be to of and a in that have i it for on with he as you for a of the'.split())

w2v_model_file_nm=usc_data_file.split('/')[-1][:-4]+'.gensimtrainedmodel'
load_model=0
if load_model==1:
    print("loading w2v embeddings")
    w2vmodel = gensim.models.Word2Vec.load(w2v_model_file_nm)
else:
    from gensim.models.word2vec import Word2Vec
    w2vmodel = Word2Vec(size=300, negative=10, min_count=1, workers=4)
    ##w2vmodel = w2vmodel.load_word2vec_format("./GoogleNews-vectors-negative300.bin.gz",binary=True)
    print('loading csv')
    uscdf=pd.read_csv(usc_data_file)
    print('Done loading csv')
    sent_list=load_sents(uscdf)
    del uscdf
    print("\tbuilding vocab")
    w2vmodel.build_vocab(sent_list)
    print("\ttraining")
    w2vmodel.train(sent_list)
    w2vmodel.save(w2v_model_file_nm)
    del sent_list

#%%

w2vmodel.most_similar(positive=['repealed'])

#%%

w2vmodel.most_similar(positive=['as','defined','in'], negative=[], topn=5)
input('ctr+c to exit, enter to continue')
#%%

ann_vec=[]
for i in range(len(anndf)):
    pred=anndf.Predicate[i]
    pred=clean_sent(pred)
    vec=w2vmodel['this']*0
    for w in pred:
        vec=vec+w2vmodel[w]
    vec=vec/len(pred)
    ann_vec.append(vec)
numpy.savetxt("ann_vec_"+ann_data_file.split('/')[-1][:-4]+".csv", ann_vec, delimiter=",")

#%%

ann_vec=[]
for i in range(len(anndf)):
    pred=anndf.Predicate[i]
    pred=clean_sent(pred)
    
    if len(pred)>1:
        pred=[w for w in pred if len(w)>3]
    if len(pred)<1:
        vec=w2vmodel['this']*0
        ann_vec.append(vec)
        print(i)
        print(anndf.Predicate[i])
        continue

    vec=w2vmodel['this']*0
    for w in pred:
        vec=vec+w2vmodel[w]
    vec=vec/len(pred)
    ann_vec.append(vec)
numpy.savetxt("ann_vec_removedlen3_"+ann_data_file.split('/')[-1][:-4]+".csv", ann_vec, delimiter=",")


