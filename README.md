Semantic Edge Labeling of Legal Citation Graphs
=======================

Citation networks are a promising recent approach to improving the intelligibility of complex rule frameworks. In a citation network (graph), rules are represented by nodes and citations are represented by edges. This repository contains an algorithm for automatic labeling of citations in a legal citation graph. it includes: A gold set of labels use to label citation types that appear in the United States Code (US Code); And the algorithm to automatically detect predicates and cluster the edges using this predefined labels. You can also find a useful graph representation tool at [PolicyNet](https://github.com/mitre/policynet).

License 
-------
This repository is released under the
[BSD license](http://www.freebsd.org/copyright/freebsd-license.html).

If you use "Semantic Edge Labeling of Legal Citation Graphs" in your research, please cite our paper,
[Semantic Edge Labeling over Legal Citation Graphs](http://law-and-big-data.org/LTDCA_2016_proceedings.pdf#page=73) from [LTDCA 2016](https://www.sandiego.edu/law/academics/continuing-legal-ed/detail.php?_focus=55039):

```
@inproceedings{sadeghian2016semantic,
  title={Semantic Edge Labeling over Legal Citation Graphs},
  author={Sadeghian, Ali and Sundaram, Laksshman and Wang, Daisy Zhe and Hamilton, William and Branting Karl and Pfeifer Craig},
  booktitle={Proceedings of the Workshop on Legal Text, Document, and Corpus Analytics},
  year={2016},
}

```

Prerequisites
-------------

 * python >= 3.5.2
 * nltk >= 3.2.1
 * pycrfsuite

Quick Start
-----------

The gold set of labels and their definitions are listed in:
```
gold label set and annotation guidelines.pdf, page 6. 
```
This document also contains specific guidelines provided to the manual annotators.

For automatic predicate tagging use:
```
./Predicate_Tagger_CRF/predicate_tagger.py
```

To generate the word embedding vectors used for clustering use:
```
./Predicate_Emebdding_And_Clustering/clustering.py
```



Acknowledgments
---------------
This work is partially supported by UF CISE Data Science Research Lab, UF Law School and ICAIR program.

Contact
-------
If you have any questions about Semantic Edge Labeling over Legal Citation Graphs, please contact [Ali Sadeghian](mailto:asadeghian@ufl.edu),
[Dr. Daisy Zhe Wang](http://www.cise.ufl.edu/~daisyw),
[DSR Lab @ UF](http://dsr.cise.ufl.edu).